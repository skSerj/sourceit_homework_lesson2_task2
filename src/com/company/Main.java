package com.company;

public class Main {

    public static void main(String[] args) {
        //задание 2:  Дано три различных числа (a, b, c). Решить квадратное уравнение ax^2 + bx + c = 0.
        // Результат вывести в консоль.

        double a = 1;
        double b = 4;
        double c = 3;

        double D = (double) Math.pow(b, 2) - 4 * a * c;

        if (D > 0) {
            double x1 = (int) ((-b + Math.sqrt(D)) / (2 * a));
            double x2 = (int) ((-b - Math.sqrt(D)) / (2 * a));
            System.out.println("Так как дискриминант 'D' больше нуля, то x1= " + x1 + "; x2= " + x2);
        } else if (D == 0) {
            double x = (-b / (2 * a));
            System.out.println("Так как дискриминант 'D' равен нулю, то x= " + x);
        } else {
            System.err.println("Так как дискриминант 'D' меньше нуля, то решений нет");
        }
    }
}
